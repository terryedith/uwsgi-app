#!/bin/bash

apt-get install build-essential python3-dev python-dev python-pip libmysqlclient-dev virtualenv*
PY="$1"
[ "$PY" == "2.7" ] && {
    PY_VER="python2.7"
    REQUIREMENTS="requirements.txt.2"
} || {
    PY_VER="python3.6"
    REQUIREMENTS="requirements.txt.3"
}
virtualenv env --no-site-packages --python=$PY_VER
TORNADO=$(cat $REQUIREMENTS | grep "tornado=\|tornado$")
[ "$TORNADO" == "" ] && {
    UWSGI_EMBED_PLUGINS="greenlet"
} || {
    UWSGI_EMBED_PLUGINS="tornado,greenlet"
}
source env/bin/activate && pip install virtualenv==16.0.0
source env/bin/activate && pip install greenlet && cp -rf env/include/site/python$PY/greenlet /usr/local/include
source env/bin/activate && UWSGI_EMBED_PLUGINS=$UWSGI_EMBED_PLUGINS pip install $TORNADO greenlet uwsgi==2.0.15
source env/bin/activate && pip install -r $REQUIREMENTS
